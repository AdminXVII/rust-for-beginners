- [Introduction](introduction.md)
    - [Installing Rust](installation.md)
    - [Cargo Package Manager](cargo.md)
    - [Familiarize Yourself With Git](git.md)
    - [The Basics](basics.md)
        - [Functions](functions.md)
        - [Assigning Values](assignments.md)
        - [Printing Outputs](printing.md)
        - [Formatting](formatting.md)
        - [What Are Types?](types.md)
        - [Heaps and Stacks](heaps_and_stacks.md)
        - [Borrows](borrows.md)
        - [Scopes](scopes.md)
        - [Structures](structures.md)
        - [Conditionals](conditionals.md)
        - [Loops](loops.md)
        - [Enumerations (ADTs)](enum.md)
        - [Methods](methods.md)
        - [Traits](traits.md)
        - [Generics](generics.md)
        - [Options & Results](option_and_result.md)
        - [Pattern Matching](pattern_matching.md)
        - [Error Handling](error_handling.md)
        - [Early Returns](early_return.md)
        - [Strings](strings.md)
        - [Vectors](vectors.md)
        - [Input Arguments](input_arguments.md)
        - [File I/O](files.md)
        - [Maps](maps.md)
        - [Recursion](recursion.md)
        - [Higher-Order Functions](higher_functions.md)
        - [Iterator Adapters](iterator.md)
        - [Option/Result Adapters](option_result_adapters.md)
        - [Threads](threads.md)
        - [Channels & Event Handling](channels.md)
        - [Atomics & Locks](locks.md)
    - [Useful Crates](useful_crates.md)
    - [Rust By Example](examples.md)
