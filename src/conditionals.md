# Conditionals

Conditionals facilitate branching in a program. Branching is when a program is given the option to choose one of many different possible paths. A conditional will decide which branch is executed.

## Conditional Operators

Values are often compared to other values with conditional operators in order to determine whether their condition is a `bool` which is either `true` or `false`. The available comparison operators are `==`, `!=`, `<`, `<=`, `>=`, and `>`.

```rust
let is_even = number % 2 == 0;
let greater_than_10 = number > 10;
```

## Conditional And Operator (&&)

The conditional and operator (`&&`) is used to evaluate an expression to `true` only if both the left and right hand expressions are true.

```rust
let condition = function1() && function2();
```

## Conditional Or Operator (||)

The conditional or operator (`||`) is used to evaluate an expression to `true` if either the left or right hand expression is true.

```rust
let condition = (function1() && function2()) || function3();
```

## Conditional Not Operator (!)

The `!` operator can be applied on booleans to reverse their state from `true` to `false`, or vice versa.

```rust
let condition = !string.is_empty();
```

## If Statement

The most common form of conditional is the **if statement**, where the first branch which evaluates to be true is expected. These are often decided by conditional operators such as . The `!` operator may also be used to convert a `false` into a `true`, or vice versa.

```rust
if value > 100 {
    println!("value is above 100");
}
```

> A branch of an if statement is also a scope, so any variables created in a branch will be dropped the moment its scope has ended.

## If With Else

Sometimes you want to execute a branch in the event that all conditional branches evaluate to `false`.

```rust
let remainder = value % 2;
if remainder == 0 {
    println!("value is evenly divisible by 2")
} else {
    println!("value is not evenly divisible: has remainder of {}", remainder);
}
```

## If With Else If

When the first condition has failed, additional conditions may also be evaluated in succession until the first `true` condition is discovered.

```rust
if argument.starts_with("this") {
    // handle this branch
} else if argument.starts_with("that") {
    // handle that branch
}
```

## If Expression

The if statement can also be used as an expression. In the below example, a value is clamped to a range between 0 and 100.

```rust
let value = if value < 0 {
    0
} else if value > 100 {
    100
} else {
    value as u32
};
```

## Match Statement

Match statements are used to evaluate equality of a value to multiple possible values. A branch defined with `_` is a placeholder which will execute if all other branches have failed.

```rust
match argument {
    "this" => {
        // this branch
    }
    "that" => {
        // that branch
    }
    _ => {
        // default branch
    }
}
```

## Match Range



## Match Guards
