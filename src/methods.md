# Methods

```rust
const PI: f32 = 3.14159265359;

fn square(input: f32) -> f32 {
    input * input
}

pub struct Circle(f32);

impl Circle {
    fn area(&self) -> f32 {
        PI * square(self.0)
    }
}

fn main() {
    println!("{}", Circle(5.0).area());
}

```

Say we want to make this more official. A float is not always guaranteed to describe a radius. Lets make it explicit that we want to crate a `Circle` type where we define the radius, and then invoke the `area()` method associated with that type.

> Newtypes can be used to term logical errors into compile-time errors.
