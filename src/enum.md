# Enumerations

Rust supports the concept of enumerations, defined with the `enum` keyword. These are types which can be represented as many possible variations. An instance of an enum will be one of the possible variants, often only known at runtime when it is checked.

```rust
enum Direction {
    West,
    North,
    East,
    South
}
```

Which can be assigned like so:

```rust
let directory = Direction::West;
```

## Algebraic Data Types

What differentiates Rust from most other programming languages is that its enums also support being used as what are commonly known as algebraic data types (ADTs), or sum types. Variants may also contain a set of values specific to that variant.

```rust
enum CartOperation {
    Add { item: Uuid, quantity: u16 },
    Remove(Uuid, u16),
    Checkout
}
```

Each of these variants can be assigned, like so:

```rust
let mut operation = CartOperation::Add { item = uuid, quantity: 1 };
operation = CartOperation::Remove(uuid, 1);
operation = CartOperation::Checkout;
```

Note that variants can be defined using either a tuple syntax, or a struct syntax. The only difference between the two are that one has named parameters, and the other does not. Named parameters are often ideal to make your variants self-describing, although they require named assignments which take longer to type when creating an instance. Use whichever works best for the situation.

## Pattern Matching on an Enum

When checking and acting upon specific variant(s) of an enum, pattern matching comes into play to make this easy. Using the `Direction` enum from the first example, pattern matching with a `match` statement would look like so:

```rust
let output: &str = match direction {
    Direction::West => "west",
    Direction::North => "north",
    Direction::East => "east",
    Direction::South => "south"
};

println!("They're going {}.", output);
```

Each branch returns a static string specific to the variant that is matched. Only one of these branches will be executed, as a value is guaranteed to be only one of the possible variants.


If you only care about matching a specific variant, you can use the `if let` statement:

```rust
if let Direction::East = direction {
    println!("They're taking the hobbits to Isengard.");
}
```

If you need to, you can also group multiple variants together for the same branch, and you can define a default branch to handle all other values.

```rust
let output = match direction {
    Direction::West | Direction::North => "either west or north",
    _ => "either east or south",
};
```

## Pattern Matching on ADT

Things get more interesting once you start working with ADTs in pattern matching. These are instrumental when describing events and states across threads through channels. More on that in the threading section.

```rust
let result = match operation {
    CartOperation::Add { item, quantity } => {
        cart.add(item, quantity)
    }
    CartOperation::Remove(item, quantity) => {
        cart.remove(item, quantity)
    }
    CartOperation::Checkout => {
        cart.checkout()
    }
};
```

## Match Ergonomics

Often times you may want to borrow an ADT so that it can be used again elsewhere.

WIP
