# Traits

```rust
use std::fmt::Display;

const PI: f32 = 3.14159265359;

fn square(input: f32) -> f32 {
    input * input
}

pub trait Area<T> {
    fn area(&self) -> T;
}

pub struct Circle(f32);

impl Area<f32> for Circle {
    fn area(&self) -> f32 {
         PI * square(self.0)
    }
}

pub struct Rectangle(f32, f32);

impl Area<f32> for Rectangle {
    fn area(&self) -> f32 {
        self.0 * self.1
    }
}

fn print_area<T: Display, A: Area<T>>(input: &A) {
    println!("{}", input.area());
}

fn main() {
    let circle = Circle(5.0);
    let rectangle = Rectangle(2.0, 4.0);

    println!(
        "{}\n{}",
        circle.area(),
        rectangle.area(),
    );

    print_area(&circle);
    print_area(&rectangle);
}

```

