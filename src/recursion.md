# Recursion

```rust
fn factorial(nth: u64) -> u64 {
    if nth == 0 {
        return 1;
    }

    nth * factorial(nth - 1)
}

fn main() {
    println!("{}", factorial(10));
}
```

Recursion is what happens when a function calls itself. It's a neat parlour trick, but each level of recursion creates a new stack, which quickly consumes all available stack space for the program, and causing it to crash. This is infamously referred to as a "stack overflow". Using a loop will always be faster.
