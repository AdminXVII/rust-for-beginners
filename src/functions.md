# Functions

Within functions, you will define statements, each ending with a `;`. Statements are defined serially, and are executed in the order that they are listed. The next statement is only executed once the previous has been completed. A statement without an `;` is treated as an expression, which generate and return values for assignment.

Each function declares that it receives and returns a set of optional input and output arguments. Functions must declare the types of the inputs and outputs, and the compiler enforces that the programmer honors these restrictions. If the function returns a value, then the value of the last expression (a statement without the ending `;`) is used as the return value.

```rust
fn name_of_function(parameter: Type1, parameter: Type2) -> Type3 {
    // First statement
    let first_value = other_function (&parameter1);

    // Second statement
    let second_value = another_function (&parameter2);

    // Final expression, whose output is the return value
    last_function (first_value, second_value)
}
```

- `fn` declares that a function is about to be defined.
- `name_of_function` is the name of this new function.
- `(parameter: Type1, parameter: Type2)` defines the input arguments for this function
- `-> Type` declares the return value, which is optional
- Within `(parameter: Type1, parameter: Type2)`
    - `parameter1` is the name of the first input argument
    - `Type1` is the type that `parameter1` will be.

## Example

We'll start with some basic math for demonstrating functions. We supply an input argument (the radius of a circle), and get an output argument back (the calculated area). We then take the value we received, and use it as an input argument for our `println!()` macro.

```rust
const PI: f32 = 3.14159265359;

fn square(input: f32) -> f32 {
    input * input
}

fn area_of_circle(radius: f32) -> f32 {
    PI * square(radius)
}

fn main() {
    println!("{}", area_of_circle(5.0));
}
```
