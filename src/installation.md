# Installing Rust

To begin your journey into programming with Rust, you must prepare your system for development. This requires installing the Rustup toolchain manager, an IDE to develop software in, and fetching the Rust API documentation locally.

## Installing Rustup

Rustup is a toolchain manager for Rust, which lets you install and manage multiple versions of Rust, components for IDE support, and cross-compilation targets. Rustup can be installed using the following curl one-liner on Linux.

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

<Screenshot of first output>

You may answer yes to all the questions, choosing the default options. Once installed, it will be import to heed the instructions it supplies at the end, which means to add the `$HOME/.cargo/bin` path to your `$PATH` so that the newly-installed binaries are accessible:


<Screenshot of last output>

```sh
source $HOME/.cargo/env
```

## Rustup IDE Components

Starting up with rustup, it is a good idea to also install the following components, which IDEs will use for real-time completions, formatting, and linting.

```sh
rustup component add clippy rls rust-analysis rust-src rustfmt
```

With that out of the way, you'll be ready to use Rust with any IDE that supports it. Most editors and IDEs support Rust today, fetching the majority of their features from the above components.

## Rust Documentation

By default, the `rust-docs` component is already installed. This contains the entirety of the Rust documentation hosted on the official website, for convenient viewing offline. The `rustup doc` command will open the offline documentation in your default web browser. If you only need access to the standard library's API documentation, you use the `--std` flag as a shortcut.

It's important to keep the API documentation open at all times while learning Rust. You may need to keep it open to reference as you develop software on a regular basis, as it will inevitably take a while before you've internalized the majority of the traits and methods available in the standard library, and specifics of their behaviors.

## Installing an IDE

The final step before you're ready to embark is to install the IDE. Most editors today work really well with Rust. The top two recommended editors for Rust are [Atom](https://atom.io/) and [VS Vode](https://code.visualstudio.com/). If on Pop!_OS, you can install Atom [here](appstream://atom), and VS Code [here](appstream://code).
