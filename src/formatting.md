# Formatting

The `println!` and `eprintln!` macros are an example of formatting macros. The first input into the macro is a string which defines the template for formatting the expression. `{}` is a placeholder for formatting a value with the `fmt::Display` trait. `{:?}` formats a value with the `fmt::Debug` trait. `{:#?}` is  pretty-printing variety of the debug trait, which is another way of saying that it makes the output more human-readable.

> See the [fmt](https://doc.rust-lang.org/std/fmt/) Rust module for details on formatting rules.

If instead of printing a value directly to an output stream, you'd rather store a string in memory, you can do this by using the `format!()` macro.

```rust
let string = format!("value of variable is {}", variable);
```

If your goal, however, is simply to convert a value into a string representing that value, it's usually better to use an existing `.to_string()` method if it exists. A method is a function which can be called directly on a value, where the first input argument to a method is the value that calls the method.

```rust
let string = number.to_string();
