# Structures

Structures are a form of type which consists of a collection of smaller types. Structures make it easier to move associated data around from within a single value. Methods can also be defined for structures, defining functionality specific to that type, and instances of it.

There are two types of structures: **fielded**, **tuple**, and **newtypes**.

## Fielded Structure

Fielded structures are the most common type of structure, where each type in the structure is a named field, and referred to by name. Each field that is given the `pub` modifier is made publicly accessible. to any who call it.

```rust
pub struct Rectangle {
    pub width: f32,
    pub height: f32,
}
```

A fielded structure can be initialized with:

```rust
let rectangle = Rectangle { width: 5.0, height: 2.5 };
```

And its fields accessed using the field accesses: `variable.field`.

```rust
println!("width: {}", rectangle.width);
println!("height: {}", rectangle.height);
```

## Tuple Structure

Tuple structures are essentially named tuples, a collection of unnamed fields referred to by their position. As with named fields, any type marked with the `pub` modifier is accessible to any caller.

```rust
pub struct Rectangle(pub f32, pub f32);
```

A tuple structure can be initialized with:

```rust
let rectangle = Rectangle(5.0, 2.5);
```

And its fields accessed in the same manner as a tuple access.

```
println!("width: {}", rectangle.0);
println!("height: {}", rectangle.1);
```

## Newtype

Newtypes are tuples with only one inner type, defining extra meaning for the value that it contains, and decreasing the likelihood of programmer error. A programmer forced to declare what a value means is less likely to use it in the wrong context. This also adds the benefit of providing special methods that aren't available to the base type that it's built from.

```rust
struct Radius(f32);
```

The same rules that apply for a tuple structure also applies for a newtype.

## Destructuring

Sometimes you will want to uniquely borrow the individual fields in a structure when working with the borrow checker. Using destructuring, it is possible to borrow specific fields in a structure, some mutably, others immutably. A destructuring which does not borrow will move the fields out of the structure, disposing of the structure.


```rust
let &mut Item { ref name, ref mut stock, ... } = &mut item;
*stock -= 1;
println!("{} ordered: {} left in stock", name, stock);
```

In the example above, the `name` field is borrowed immutable, and the `stock` field is borrowed mutably. All other fields are ignored, as indicated by the `...`.
