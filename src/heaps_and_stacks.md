# Heaps and Stacks

You may run across this terminology frequently in the Rust community. To understand heaps and stacks, you must first understand memory and allocations. Mastery of this knowledge is key to developing high-performance applications that use little memory.

## Memory

Every system has a set amount of memory, based on the capacity of the RAM installed onto the machine. Memory is where values are stored. The amount of memory on a system will define how many values that your system can store before it runs out of memory.

## Allocations

An allocation is when a region of memory is assigned to a process. When making an allocation, the process must tell a memory allocator how much memory that it wants to request. That allocator is then responsible for finding a region of unused memory large enough to give back to the process. The process can later then tell the memory allocator that it no longer needs the allocated memory, and the memory is given back to the OS.

## The Stack

When a new process is executed on the OS, the OS allocates a predefined finite region of memory to the process, called the stack. Memory on the stack is often much faster to access than on the heap. This stack belongs exclusively to the process that it was assigned to, and it is where all variables are stored when they are created by the program. When a program runs out of stack memory, a program crashes with what's commonly known as a "stack overflow".

## The Heap

Often times you will need to interact with values of unknown or excessive length. This is when the heap comes into play. Applications can request for additional memory through a memory allocator. The good news is that it keeps your stack lean and gives your program access to a huge pool of memory. The downside is that every memory allocation and free consumes a lot of time, and is often much slower to access than the stack.

> In Rust's standard library, all of the `std::collection` types are based on heap-allocating the values that they own. This includes `String`, `PathBuf`, `Vec`, `HashMap`, and `BTreeMap`, as an example. However, note that these types do not allocate anything by default when their `new()` constructor is called.

## Arenas / Pools

There are some strategies for handling memory on the heap effectively, in order to mitigate the costs of repeat allocations and frees. Arenas, also known as pools, can pre-allocate a sizeable region of memory for a predetermined type that the program can hold onto and reuse. See the [generational-arena](https://lib.rs/crates/generational-arena) and [slotmap](https://lib.rs/crates/slotmap) crates as examples of arenas.
