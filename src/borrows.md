# Borrows

Now that you know how to assign a variable, you'll need to learn how to borrow them. Borrows are useful to give access to a value to another location. Values can only exist in one location at a time, so to make them accessible from another, you can create a reference using a reference operator (`&`). The dereference operator (`*`) can be used to access the value behind the reference.

```rust
let value: u32 = 5;
let reference: &u32 = &variable;
let new_value: u32 = *reference;
```

## How References Work

References are pointers to the memory address of a value, whereby the type of the value at that address is known by the compiler. For most types, references are just a pointer. The exception to this rule are array references; also known as slices, or fat pointers; which also contain an integer indicating the number of elements being referred to in the array.

Pointers are integers that point to a specific position in memory. The size of a pointer is dependent on the environment, so a 64-bit kernel passes around 64-bit integers as pointers, whereas a 32-bit kernel passes around 32-bit integers as pointers.

## Reference Mutability

Similar to the difference between immutable and mutable variables, this distinction also exists for references. Borrowing a variable with `&` creates an jimmutable reference, whereas borrowing a vvariable with `&mut` creates a mutable reference. The mutability determines whether you're allowed to modify the value that the reference points to.

```rust
let variable: u32 = 0;
let mutable: &mut u32 = &mut variable;
*mutable = 5;
```

## Borrowing Rules

One of the major features in Rust, as compared to C and C++, is the borrow checker. The borrow checker adds restrictions to borrows to prevent potentially-unsafe behaviors, such as a reference pointing to an address of a value which moved and longer exists. The borrow checker's rules can boiled down to:

- Unlimited immutable shared references (`&`) can exist at a given time.
- Only one mutable unique reference (`&mut`) can exist at a given time.
- If an immutable reference exists, then a mutable reference cannot be created.
- You may, however, coerce a mutable reference to an immutable one.

Observe these rules, and the borrow checker will not give you a hard time. Throughout this book, we will demonstrate patterns that you can use to solve every kind of problem that you'll experience with borrowing.
