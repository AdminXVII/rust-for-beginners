# Input Arguments, Iterators, & Options

Programs aren't very useful if they aren't able to receive inputs. So we'll start practicing with input arguments now.

```sh
cargo new arguments
code arguments
```

All programs launched will contain an array of strings containing the arguments that were given to the program when it was called. The first argument in this array is always the name of the program that was invoked, and then all arguments that follow were defined by the caller that invoked the program. This argument array can be fetched using the `std::env::args()` function from the `std::env` module.

## Importing Namespaces

In order to use that `args()` function from the `std::env` module, we will need to import that function from the `env` namespace. This can be done using the `use` keyword, like so:

```rust
use std::env::args;
```

## Creating an Args value

The definition of the `args()` function is `fn() -> Args`, which means that it returns the `Args` type when it is called. If you do a search for `Args` in the Rust API documentation, you'll find all the details about this type, the methods that you can call from it, and the traits that it implements.

```rust
fn main() {
    let mut arguments = args();
}
```

## Options & Generics

Before we dive into the `Args` type, it will be important to understand the `Option` type. The `Option` type is an example of an **enum**, which in academics is known by a handful of different names: most commonly sum types and algebraic data types (ADTs). Put simply, it is a type which can be interpreted as one of many possible variations, but you will only know which variant a type is once you check it.

```rust
enum Option<T> {
    Some(T),
    None
}
```

The `<T>` in `Option<T>` defines that `Option` is a **generic** type, where `T` is a placeholder for any type. By defining that this type holds a generic type named `T`, it is possible to use `T` in the definition of that type at `Some(T)`. `Option<String>` and `Option<u64>` would be two entirely different instances of the `Option` type, but both share the same underlying semantics.

## Iterators (Basic)

The most important thing to know about the `Args` type is that it implements the `Iterator` trait. Types which implement this trait support a wide number of methods for fetching specific values from a iterator, as well as modifying them as they are fetched. Given that the first element in the argument array is the name of the program, we can tell the `Args` iterator to `skip(1)` it, which will advance the iterator by one, ignoring the first element.

```rust
let mut arguments = args();
arguments.skip(1);
```

We can then manually fetch values from the iterator using the `next()` method, as defined in the `Iterator` trait, as seen in its definition below:

```rust
pub trait Iterator {
    type Item;

    fn next(&mut self) -> Option<Self::Item>;
}
```

In `Args`, the `Item` type is defined as `String`, like so:


```rust
impl Iterator for Args {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        ...
    }
}
```

Which means that each call to `next()` on an `Args` value will return an `Option<String>`, whose state will either be `Some(String)`, or `None`.

```rust
let first: String = arguments.next().expect("missing first argument");
let second: String = arguments.next().expect("missing second argument");
let third: String = arguments.next().expect("missing third argument");
```



## Unwrapping and Panics

It is considered best practice to perform some error handling by accounting for the possibility that there is no value being returned. However, for the case of example, or when you know for certain that a value should always exist, you can use the `unwrap()` or `expect("description")` methods to extract the `Some(T)`, or otherwise panic with an error message on `None`.

It is advisable to always use `expect()` for the `Option` type, as the error message on panic will likely be impossible to track down.


## For Loops

To work with arguments, we're going to introduce the concept of loops. Loops are statements which repeat an action until a condition is met that "breaks" it from the loop. In the case of handling arguments, the break condition is when all arguments have been read. The statements to execute for each iteration of a loop are enclosed in a new scope defined with the `{}` braces.

The most common form of loops is the `for` loop, which reads like `for variable in values`, where the left hand side of `in` defines variables which are defined by each iteration of the values returned by the right hand side.

In the example below, you'll see that our for loop uses the `args()` iterator as its input. Each iterator will invoke the `next()` method, until all values have been read.

```rust
use std::env::args;

fn main() {
    for argument in args() {
        println!("{}", argument);
    }
}
```

Try running this with `cargo run 1 2 3`. Note how the first line that is printed contains the path of the binary that was executed.
