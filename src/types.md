# What Are Types?

All values in Rust must have a defined type. An example of a type would be a cat, or a dog. Both of these are types of animals. All cats are animals, but not all animals are cats. The same is true in programming. A software type defines what a set of bytes are. All strings are bytes, but not all bytes are strings.

The most important types are the primitive types. Refer to the [primitives](https://doc.rust-lang.org/std/#primitives) section of the Rust standard library for a complete list of primitive types. These are the fundamental types from which all complex types are made of. Like atoms in a molecule.

Of the primitive types, there are a few categories:

## String

Text is represented in software as strings.

```rust
let string: &str = "hello, new programmer";
```

> Strings in Rust are UTF-8, which means they consist of a slice of `u8` integers, but combined represent a collection of characters that make up words, sentences, and so on. Each character may have a different length, from a `u8` to a `u32`.

## Character

Represents a single character, which may be from the roman alphabet, or any other alphabet, including emojis.

```rust
let character: char = 'A';
```

> In Rust terms, this is a "unicode scalar value". Important to note that an array of characters does not make a string, at least not of the UTF-8 variety.

## Boolean

Represents either `true` or `false`. Either zero or non-zero.

```rust
let boolean: bool = true;
```

## Bytes

A byte is the smallest addressable unit. They represent eight individual bits. Look into bitwise math if you want to operate on the individual bits inside of one of these units.

```rust
let byte: u8 = 0;
```

> - UTF-8 strings consist of slices of bytes.
> - Booleans are actually bytes in disguise

## Integers

Integers are whole numbers which may or may not be "signed". Signed integers can be negative, whereas unsigned integers can only be positive. The size of an integer defines the minimum and maximum value that it can represent.

```rust
let integer: u64 = 152;
```

## Pointers

These are integers which point to the location of a memory address. Pointers are safe to create and modify, but unsafe to access the location that they point to, as seen above.

```rust
let ptr: *const u64 = &152;
let integer: u64 = unsafe { *ptr };
```

## Reference

The safe alternative to pointers. These are tracked by the compiler and are guaranteed to point to valid memory addresses.

```rust
let reference: &u64 = &152;
let integer: u64 = *reference
```

## Floating Point

Floating point numbers are numbers which can represent a fraction of a whole number. These are more expensive than integers to calculate, so integers should be used when possible.

```rust
let float: f32 = 5.5;
```

There are two types of floating point types:

- floating binary (`f32`, `f64`): inaccurate, but fast to calculate. **Not for currency.**
- floating decimal (`d128`): accurate, but slower. **Useful for currency.**

Rust only provides floating binary primitives by default. Crates can be imported to gain access to decimal variants. Inclusion in the standard library, you ask? Maybe one day.

## Arrays & Slices

Arrays are a collection of elements of a specific type. Arrays have a known width at compile time. Arrays can be sliced and passed around as slices, which have an unknown width at compile time. In Rust all slices are "fat pointers", which is a pointer that points to the first element in the array, and is accompanied by an integer that defines the number of elements in that array.

```rust
let array: [u32; 5] = [0, 1, 2, 3, 4];
let slice: &[u32] = &array[2..];
```

## Tuples

Tuples are similar to arrays, but they do not have to consist of the same type. A tuple is merely a collection of values that the compiler mashes into one large value, but is able to interpret the individual elements from that conglomerated value.

```rust
("string", 5.5, 24)
```
