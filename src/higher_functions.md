# Higher-Order Functions

```rust
const PI: f32 = 3.14159265359;

fn square(input: f32) -> f32 {
    input * input
}

fn area_of_circle(radius: f32) -> f32 {
    PI * square(radius)
}

fn calculate_result(
    func1: fn(f32) -> f32,
    func2: fn(f32) -> f32,
    input: f32
) -> f32 {
    func1(input) + func2(input)
}

fn main() {
    let result = calculate_result(square, area_of_circle, 5.0);
    println!("{}", result);
}
```

Functions may also be used as input and output parameters themselves, referred to by academics as "higher-order functions". This example in particular is based on function pointers, which are merely memory addresses that point to the location of the function. Rust validates that the function signature at that address is a match to what the function requests.
