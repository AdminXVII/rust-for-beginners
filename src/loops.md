# Loops

Actions which can be performed multiple times, often

## For Range Loops

```rust
for number in 0..=100 {
    println!("{}", number);
}
```

## Break and Continue

The **break** and **continue** keywords are used to construct a loop to either stop looping, or to skip the current iteration and continue with the next.

```rust
for number in 0..=100 {
    if number.is_odd() {
        continue
    } else if number > 20 {
        break
    }

    println!("{}", number);
}
```

## For Iterable Loop

Any value which implements the `Iterator` trait can be used in a for loop. Ranges are also a form of iterable type.

```rust
for element in vector {
    println!("{}", element);
}
```

## For Iterable Borrow

However, note that the above example will consume all of the values from the `vector`, thus destroying it. Iterable types which support it, such as the `Vec` type, can iterate through a borrow, which allows retaining the input value, and optionally the ability to modify elements in the borrow.

```rust
let mut found = false;
for element in &vector {
    if *element == "this"
        found = true;
        break;
    }
}
```

```rust

```

## While Loops

While loops continue to iterate until the while condition is `false`.

```rust
while condition_is_true() {
    do_thing();
}
```

## Infinite Loops

This is equivalent to a **while** loop which always evaluates to `true`. This loop will only break if the **break** keyword is used to break out from a loop.

```rust
loop {
    if do_thing() {
        break
    }
}
```

## Labeled Loops
