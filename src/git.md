# Familiarize Yourself With Git

When initializing a new project, Cargo will also initialize git versioning control for you, along with a pre-generated `.gitignore` file. You can see a list of files that have changed since the last commit was made with the `status` subcommand of `git`.

```sh
git status
```

Add all of these files, and then commit them.

```sh
git add *
git commit -m ':tada: Initialized first project

Description of the plan for the new project.'
```

## Add Remote & Push Changes Upstream

If you have a repository on GitHub or GitLab, you can add the remote for it and push it to the web.

```sh
git remote add origin 'URL'
git push origin master
```

## Pulling Upstream Changes

If you need to pull changes from the upstream:

```sh
git pull origin master
```

## Commit Amendments

If you ever need to make a few changes and add them to the last commit you made, you can run

```sh
git commit --amend
git push origin master --force
```

Try not to force push too often. It's okay if it's on a branch, but it gets annoying to other contributors if you do this directly to the `master` branch. You can create a branch like so:

## Branch Creation

```sh
git checkout -b new-branch-name
# do thing
git checkout push origin new-branch-name
```

## Branch Switching

And switch to another branch with

```sh
git checkout master
```

## Fetching Upstream Branches

Fetching a branch from an upstream repo? Easy

```sh
git fetch origin
git checkout branch-on-repo
```
