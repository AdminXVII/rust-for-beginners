# Printing Outputs

Lets create a new project named `outputs`, and open it in our editor. Substitute `code` for the editor that you've chosen.

```sh
cargo new outputs
code outputs
```

## Main Function

The entry point for a Rust application is the `main()` function in the `main.rs` file. So we will now open the file at `src/main.rs`, and then delete what exists here, and start with an empty `main()` function.

```rust
fn main() {

}
```

## Output Streams

Starting out, it's important to learn about output streams, particularly "standard output" and "standard error". Every program essentially starts with two mouths: one explcitly for communicating errors, warnings, and other annoying information; and one for communicating information that's important. The person calling the program gets to decide whether they listen to both at the same time, one or the other, or neither.

```rust
fn main() {
    println!("I am speaking to {}", "standard output");
    eprintln!("I am speaking to {}", "standard error");
}
```

In the example above, `println!()` will print the given string to standard output, and `eprintln!()` will print its given string to standard error. You can compile this with `cargo build`, and run it in a terminal with `target/debug/outputs`.

If you want to experiment with ignoring streams, try these out:

```sh
target/debug/outputs 2>/dev/null
target/debug/outputs 1>/dev/null
target/debug/outputs &>/dev/null
```

```

## Bonus: Under the Hood

So how do those macro work? File descriptors, and the `io::Write` trait. It's okay to not fully understand this example right now. This is a sneak peek into some low level details that are hidden behind our macro abstractions.

> File descriptors are numbers that represent open files in a program. File descriptor 0 is standard input, file descriptor 1 is standard output, and file descriptor 2 is standard eror. As for traits, they are are essentially behaviors that are shared among types. They must be imported in order to use their methods. More on this later.


```rust
use std::io::{self, Write};

fn main() {
    {
        // Lock access to the stdout file descriptor.
        let stdout = io::stdout();
        let mut stdout_locked = stdout.lock();

        // Write directly to it with the Write trait.
        stdout_locked.write(b"I am speaking to ");
        stdout_locked.write(b"standard output");
        stdout_locked.write(b"\n");

        // Drop the lock.
    }

    {
        // Do the same with stderr's file descriptor.
        let stderr = io::stderr();
        let mut stderr_locked = stderr.lock();

        stderr_locked.write(b"I am speaking to ");
        stderr_locked.write(b"standard error");
        stderr_locked.write(b"\n");

        // Drop the lock
    }
}
```
